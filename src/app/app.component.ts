import { Component } from '@angular/core';

export interface Tile {
  count: string;
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cdc';
  tiles: Tile[] = [
    {text: '1', count: '5'},
    {text: '2', count: '0'},
    {text: '3', count: '10'},
    {text: '4', count: '2'},
    {text: '5', count: '2'},
    {text: '6', count: '0'},
    {text: '7', count: '0'},
    {text: '8', count: '0'},
    {text: '9', count: '10'},
    {text: '10', count: '12'},
    {text: '11', count: '1'},
    {text: '12', count: '0'},
    {text: '13', count: '5'},
    {text: '14', count: '1'},
    {text: '15', count: '0'},
    {text: '16', count: '0'},
    {text: '17', count: '0'},
    {text: '18', count: '0'},
    {text: '19', count: '25'},
    {text: '20', count: '2'},
    {text: '21', count: '0'},
    {text: '22', count: '0'},
    {text: '23', count: '4'},
    {text: '24', count: '0'},
    {text: '25', count: '1'},
    {text: '26', count: '1'},
    {text: '27', count: '1'},
    {text: '28', count: '2'},
    {text: '29', count: '0'},
    {text: '30', count: '18'},
    {text: '31', count: '3'},
  ];
}
